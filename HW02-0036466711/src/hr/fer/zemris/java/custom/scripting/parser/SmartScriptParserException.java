package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Thrown when the document isn't parsed correctly.
 * @author Mihovil Vinković
 *
 */
public class SmartScriptParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
